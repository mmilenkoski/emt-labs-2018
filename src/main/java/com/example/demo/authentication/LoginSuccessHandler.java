package com.example.demo.authentication;
//import mk.ukim.finki.emt.events.UserRegisteredEvent;

import com.example.demo.model.Role;
import com.example.demo.model.User;
import com.example.demo.persistence.UserRepository;
import com.example.demo.model.Provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by ristes on 3/15/16.
 */
public class LoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

  ApplicationEventPublisher publisher;
  private Provider provider;
  private Role defaultUserRole;
  @Autowired
  private UserRepository userRepository;

  private User user;

  public LoginSuccessHandler(Provider provider, Role defaultUserRole, ApplicationEventPublisher publisher) {
    this.provider = provider;
    this.defaultUserRole = defaultUserRole;
    this.publisher = publisher;
  }

  @Override
  public void onAuthenticationSuccess(
    HttpServletRequest httpServletRequest,
    HttpServletResponse httpServletResponse,
    Authentication authentication
  ) throws IOException, ServletException {

    HttpSession session = httpServletRequest.getSession();
    User user = getUser(authentication);
    session.setAttribute("user", user);

    super.onAuthenticationSuccess(httpServletRequest, httpServletResponse, authentication);
  }

  public User getUser(Authentication authentication) {
    User user = userRepository.findByUsername(authentication.getName());
    if (user == null) {
      user = createUserFromProvider(authentication);
    }
    return user;
  }

  private User createUserFromProvider(Authentication authentication) {

    user = new User();
    String name = authentication.getName();
    name = name.replace(" ", "");
    name = name.toLowerCase();
    user.username = name + "@finki.ukim.mk";
    user.role = defaultUserRole;
    user.provider = provider;
    userRepository.save(user);
    //publisher.publishEvent(new UserRegisteredEvent(user));
    return user;
  }
}

