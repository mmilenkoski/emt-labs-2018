package com.example.demo.init;

import com.example.demo.model.Gender;
import com.example.demo.model.Role;
import com.example.demo.model.User;
import com.example.demo.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Component
public class UserInit {
    private static Logger logger = LoggerFactory.getLogger(UserInit.class);

    private final UserService userService;

    private final PasswordEncoder passwordEncoder;

    private final Environment env;

    @Value("${app.user.admin.password}")
    private String adminPassword;

    public UserInit(UserService userService, PasswordEncoder passwordEncoder, Environment env) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.env = env;
    }

    @PostConstruct
    public void init() {
        logger.info("Initializing user");
        if (userService.numberOfUsers() == 0) {
            User user = new User();
            user.username = env.getProperty("app.user.admin.email");
            //user.username = env.getProperty("app.user.admin.username");
            user.password = passwordEncoder.encode(adminPassword);
            user.firstName = "ADMIN";
            user.lastName = "ADMIN";
            user.gender = Gender.MALE;
            user.birthDate = LocalDate.now();
            user.activated = true;
            user.role = Role.ROLE_ADMIN;
            userService.save(user);

        }

    }
}
