package com.example.demo.config;


import com.example.demo.authentication.LoginFailureHandler;
import com.example.demo.authentication.LoginSuccessHandler;
import com.example.demo.authentication.OAuth2TokenService;
import com.example.demo.authentication.OAuthClientResource;
import com.example.demo.model.Provider;
import com.example.demo.model.Role;
import com.example.demo.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter;
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.session.SessionManagementFilter;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.filter.CompositeFilter;

import javax.servlet.Filter;
import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableOAuth2Client
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Bean
    @Order(0)
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }

    @Autowired
    CustomUserDetailsService userDetailsService;

    @Autowired
    ApplicationEventPublisher publisher;

    @Qualifier("oauth2ClientContext")
    @Autowired
    OAuth2ClientContext oauth2ClientContext;

    @Override
    protected void configure(
            AuthenticationManagerBuilder auth) throws Exception {
        super.configure(auth);
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                //
                .csrf().disable()
                // da si dodadam moja login page, mesto predefinirana
                .formLogin().loginPage("/login")
                .failureHandler(loginFailureHandler())
                .successHandler(localSuccessHandler())
                .permitAll()
                .and()
                // da dodadam rememberMe svojstvo
                .rememberMe().key("uniqueRembemberMeKey").tokenValiditySeconds(2592000)
                .and()
                .logout().clearAuthentication(true).invalidateHttpSession(true).deleteCookies("remember-me")
                .and()
                .authorizeRequests().antMatchers("/profile", "/chatroom").hasAnyRole("EMPLOYEE", "ADMIN", "MANAGER")
                .and()
                .authorizeRequests().antMatchers("/users").hasAnyRole("ADMIN", "MANAGER")

                //.and()
                //.authorizeRequests().antMatchers("/edit-profile-change-password").hasAuthority("CHANGE_PASSWORD_AUTHORITY")
                .and()
                .authorizeRequests().antMatchers("/*").permitAll()
                .and()
                .addFilterBefore(ssoFilter(), BasicAuthenticationFilter.class);
        //http.addFilterAfter(oauth2AuthenticationFilter(), SessionManagementFilter.class);

    }

    @Bean
    public AuthenticationFailureHandler loginFailureHandler() {
        return new LoginFailureHandler();
    }

    @Bean
    public AuthenticationSuccessHandler localSuccessHandler() {
        return new LoginSuccessHandler(
                Provider.LOCAL,
                Role.ROLE_EMPLOYEE,
                publisher
        );
    }

    @Bean
    @ConfigurationProperties("github")
    OAuthClientResource github() {
        return new OAuthClientResource();
    }


    @Bean
    LoginSuccessHandler githubSuccessHandler() {
        return new LoginSuccessHandler(Provider.GITHUB, Role.ROLE_EMPLOYEE, publisher);
    }


    @Bean
    @ConfigurationProperties("google")
    OAuthClientResource google() {
        return new OAuthClientResource();
    }


    @Bean
    LoginSuccessHandler googleSuccessHandler() {
        return new LoginSuccessHandler(Provider.GOOGLE, Role.ROLE_EMPLOYEE, publisher);
    }


//    @Bean
//    @ConfigurationProperties("facebook")
//    OAuthClientResource facebook() {
//        return new OAuthClientResource();
//    }


    @Bean
    LoginSuccessHandler facebookSuccessHandler() {
        return new LoginSuccessHandler(Provider.FACEBOOK, Role.ROLE_EMPLOYEE, publisher);
    }

//    private Filter ssoFilter() {
//
//        CompositeFilter filter = new CompositeFilter();
//        List<Filter> filters = new ArrayList<Filter>();
//
//        filters.add(
//                getOauthFilter(
//                        "/login/facebook",
//                        facebook(),
//                        facebookSuccessHandler()
//                )
//        );
//
//
//        filters.add(
//                getOauthFilter(
//                        "/login/github",
//                        github(),
//                        githubSuccessHandler()
//                )
//        );
//
//        filters.add(
//                getOauthFilter(
//                        "/login/google",
//                        google(),
//                        googleSuccessHandler()
//                )
//        );
//
//        filter.setFilters(filters);
//        return filter;
//
//    }

    @Bean
    @ConfigurationProperties("facebook.client")
    public AuthorizationCodeResourceDetails facebook() {
        return new AuthorizationCodeResourceDetails();
    }

    @Bean
    @ConfigurationProperties("facebook.resource")
    public ResourceServerProperties facebookResource() {
        return new ResourceServerProperties();
    }

    private Filter ssoFilter() {
        OAuth2ClientAuthenticationProcessingFilter facebookFilter = new OAuth2ClientAuthenticationProcessingFilter("/login/facebook");
        OAuth2RestTemplate facebookTemplate = new OAuth2RestTemplate(facebook(), oauth2ClientContext);
        facebookFilter.setRestTemplate(facebookTemplate);
        UserInfoTokenServices tokenServices = new UserInfoTokenServices(facebookResource().getUserInfoUri(), facebook().getClientId());
        tokenServices.setRestTemplate(facebookTemplate);
        facebookFilter.setTokenServices(tokenServices);
        return facebookFilter;
    }

    private Filter oauth2AuthenticationFilter() throws Exception {
        OAuth2AuthenticationProcessingFilter filter = new OAuth2AuthenticationProcessingFilter();
        filter.setStateless(false);
        filter.setAuthenticationManager(authenticationManager());
        return filter;
    }

    private Filter getOauthFilter(
            String loginUrl,
            OAuthClientResource client,
            AuthenticationSuccessHandler successHandler) {

        OAuth2ClientAuthenticationProcessingFilter oauthFilter =
                new OAuth2ClientAuthenticationProcessingFilter(loginUrl);
        OAuth2RestTemplate template = new OAuth2RestTemplate(client.getClient(), oauth2ClientContext);
        oauthFilter.setRestTemplate(template);

        //service used to obtain/create the authentication object
        OAuth2TokenService tokenService = new OAuth2TokenService(
                client.getResource().getUserInfoUri(),
                client.getClient().getClientId()
        );
        oauthFilter.setTokenServices(
                tokenService
        );
        oauthFilter.setAuthenticationSuccessHandler(successHandler);
        return oauthFilter;
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder;
    }


    @Bean
    public FilterRegistrationBean oauth2ClientFilterRegistration(
            OAuth2ClientContextFilter filter) {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(filter);
        registration.setOrder(-100);
        return registration;
    }


//    @Bean
//    @ConfigurationProperties("github")
//    public ClientResources github() {
//        return new ClientResources();
//    }
//
//    @Bean
//    @ConfigurationProperties("google")
//    public ClientResources google() {
//        return new ClientResources();
//    }
//
//    @Bean
//    @ConfigurationProperties("facebook")
//    public ClientResources facebook() {
//        return new ClientResources();
//    }

//    private Filter ssoFilter() {
//        CompositeFilter filter = new CompositeFilter();
//        List<Filter> filters = new ArrayList<>();
//        filters.add(ssoFilter(facebook(), "/login/facebook"));
//        filters.add(ssoFilter(github(), "/login/github"));
//        filters.add(ssoFilter(google(), "/login/google"));
//        filter.setFilters(filters);
//        return filter;
//    }
//
//    private Filter ssoFilter(ClientResources client, String path) {
//        OAuth2ClientAuthenticationProcessingFilter oAuth2ClientAuthenticationFilter = new OAuth2ClientAuthenticationProcessingFilter(path);
//        OAuth2RestTemplate oAuth2RestTemplate = new OAuth2RestTemplate(client.getClient(), oauth2ClientContext);
//        oAuth2ClientAuthenticationFilter.setRestTemplate(oAuth2RestTemplate);
//        UserInfoTokenServices tokenServices = new UserInfoTokenServices(client.getResource().getUserInfoUri(),
//                client.getClient().getClientId());
//        tokenServices.setRestTemplate(oAuth2RestTemplate);
//        oAuth2ClientAuthenticationFilter.setTokenServices(tokenServices);
//        return oAuth2ClientAuthenticationFilter;
//    }

    class ClientResources {

        @NestedConfigurationProperty
        private AuthorizationCodeResourceDetails client = new AuthorizationCodeResourceDetails();

        @NestedConfigurationProperty
        private ResourceServerProperties resource = new ResourceServerProperties();

        public AuthorizationCodeResourceDetails getClient() {
            return client;
        }

        public ResourceServerProperties getResource() {
            return resource;
        }
    }

}
