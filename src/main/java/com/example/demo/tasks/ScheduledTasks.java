package com.example.demo.tasks;

import com.example.demo.model.User;
import com.example.demo.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class ScheduledTasks {

    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Autowired
    private UserService userService;

    @Scheduled(fixedRate = 86400000)
    //@Scheduled(fixedRate = 60000)
    public void reportCurrentTime() {
        log.info("Deleting not activated users");
        Iterable<User> users = userService.findAll();
        List<User> toDelete = new ArrayList<>();
        for (User u : users){
            if (!u.isActivated()){
                toDelete.add(u);
            }
        }
        for (User u: toDelete){
            userService.removeUser(u.username);
        }
    }
}
