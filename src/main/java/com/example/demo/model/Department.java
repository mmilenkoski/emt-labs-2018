package com.example.demo.model;

import javax.persistence.*;

@Entity
public class Department {

    @Id
    @GeneratedValue
    public Long id;

    public String name;
}
