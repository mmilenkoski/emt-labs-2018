package com.example.demo.model;



import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter @Setter
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int id;


    public String firstName;
    public String lastName;

    public String username;
    public String password;

    public Gender gender;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public LocalDate birthDate;
    public LocalDateTime registrationDate;

    public boolean activated;
    public String activationCode;

    public Role role;

    @ManyToOne
    public Department department;

    @Enumerated(EnumType.STRING)
    public Provider provider;


//    public String getUsername() {
//        return username;
//    }
//
//    public String getPassword() {
//        return password;
//    }
//
//    public Role getRole() {
//        return role;
//    }
}
