package com.example.demo.model;

public enum Role {
    ROLE_EMPLOYEE, ROLE_MANAGER, ROLE_ADMIN
}
