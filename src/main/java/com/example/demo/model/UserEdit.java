package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

//@PasswordMatches
@Getter
@Setter
public class UserEdit {
    @NotNull
    @NotEmpty
    public String username;

    @NotNull
    @NotEmpty
    public String firstName;

    @NotNull
    @NotEmpty
    public String lastName;

    @NotNull
    @NotEmpty
    public String gender;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public LocalDate birthDate;

//    @NotNull
//    //@DateTimeFormat(pattern = "yyyy-MM-dd")
//    public LocalDateTime registrationDate;
}
