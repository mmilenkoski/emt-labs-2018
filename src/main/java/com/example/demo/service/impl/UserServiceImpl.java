package com.example.demo.service.impl;

import com.example.demo.model.*;
import com.example.demo.persistence.UserRepository;
//import com.example.demo.service.DepartmentService;
import com.example.demo.service.DepartmentService;
import com.example.demo.service.UserService;
import groovyjarjarcommonscli.GnuParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Random;

@Service
public class UserServiceImpl implements UserService {


    private final UserRepository userRepository;
    private final DepartmentService departmentService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           DepartmentService departmentService,
                           PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.departmentService = departmentService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User registerNewUser(UserDto user) {
//        if(this.checkDuplicateEmail(user.getEmail())){
//            throw new DuplicateEmailException();
//        }
        Department department = departmentService.findByName(user.department);
        User u = new User();
        String encodedPass = passwordEncoder.encode(user.getPassword());
        u.setFirstName(user.getFirstName());
        u.setLastName(user.getLastName());

        u.setUsername(user.getEmail());
        //u.setEmail(user.getEmail());
        u.setPassword(encodedPass);
        //u.setGender(user.getGender());
        String gender = user.getGender();
        if (gender.equals("MALE")){
            u.setGender(Gender.MALE);
        }
        if (gender.equals("FEMALE")){
            u.setGender(Gender.FEMALE);
        }
        // TODO: change birthDate
        u.setBirthDate(user.birthDate);
        u.setRegistrationDate(LocalDateTime.now());
        u.setActivationCode(getActivationCode());
        u.setRole(Role.ROLE_EMPLOYEE);
        u.setActivated(false);
        u.setDepartment(department);
        return userRepository.save(u);
    }





    @Override
    public boolean activateUser(String username, String activationCode) {
        User u = userRepository.findByUsername(username);
        if (u.activationCode.equals(activationCode)){
            u.activated = true;
            userRepository.save(u);
            return true;
        }
        return false;
    }

    @Override
    public String changePassword(String username) {
        User u = userRepository.findByUsername(username);
        if (u != null){
            String password = getPassword();
            //User user = u.get();
            u.password = passwordEncoder.encode(password);
            userRepository.save(u);
            return password;
        }
        return null;
    }

    @Override
    public String resetPassword(User user, String newPassword, String matchingPassword) {
        if (newPassword.equals(matchingPassword)){
            user.password = passwordEncoder.encode(newPassword);
            userRepository.save(user);
            return newPassword;
        }
        return null;
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public Page<User> findAll(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @Override
    public void updateUser(String username, String firstName, String lastName, String gender, LocalDate birthDate) {
        User user = findByUsername(username);
        user.firstName = firstName;
        user.lastName = lastName;
        user.gender = gender.equals("MALE") ? Gender.MALE : Gender.FEMALE;
        user.birthDate = birthDate;
        userRepository.save(user);
    }

    @Override
    public void removeUser(String username) {
        if (!username.endsWith(".com")){
            username = username + ".com";
        }
        User user = findByUsername(username);
        userRepository.delete(user);
    }

    @Override
    public Page<User> findAll(Example<User> example, Pageable pageable) {
        return userRepository.findAll(example, pageable);
    }

    @Override
    public long numberOfUsers() {
        return userRepository.count();
    }

    protected String getPassword() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toLowerCase();
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }

    protected String getActivationCode() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
}
