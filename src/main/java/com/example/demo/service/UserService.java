package com.example.demo.service;

import com.example.demo.model.User;
import com.example.demo.model.UserDto;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.jws.soap.SOAPBinding;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface UserService {

    //User registerNewUser(UserDto user) throws DuplicateEmailException;
    User registerNewUser(UserDto user);

    boolean activateUser(String username, String activationCode);

    String changePassword(String username);

    String resetPassword(User user, String newPassword, String matchingPassword);

//    User saveUser(UserDto user);
//
    User save(User user);


//
//    User getUser(String token);
//
//    //UserVerificationToken getToken(String token);
//
//    void createVerificationToken(User user, String randomToken);
//
//    //UserVerificationToken generateNewVerificationToken(String verificationToken);
//
//    String validateVerificationToken(String token) ;
//
    User findByUsername(String username);
    Iterable<User> findAll();

    Page<User> findAll(Pageable pageable);

    void updateUser(String username, String firstName, String lastName, String gender, LocalDate birthDate);

    void removeUser(String username);
    Page<User> findAll(Example<User> example, Pageable pageable);

    long numberOfUsers();
//
//    Optional<User> findByEmail(String email);
//
//    User findById(Long id);
//
//    void createPasswordResetTokenForUser(User user, String token);
//
//    String validatePasswordResetToken(Long id, String token);
//
//    void changeUserPassword(User user, String password);
//
//    long numberOfUsers();

}

