package com.example.demo.service;

import com.example.demo.model.Department;

public interface DepartmentService {
    Department create(Department department);

    Iterable<Department> findAll();

    Department findByName(String name);
}
