package com.example.demo.resource;

import com.example.demo.config.thymeleaf.Layout;
import com.example.demo.model.*;
import com.example.demo.registration.OnRegisterCompletedEvent;
import com.example.demo.service.DepartmentService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.*;
import org.springframework.http.MediaType;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.jws.soap.SOAPBinding;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Controller
public class HelloResource {


    private final UserService userService;
    private final DepartmentService departmentService;
    private final ApplicationEventPublisher eventPublisher;
    private final JavaMailSender mailSender;

    @Autowired
    public HelloResource(UserService userService,
                         DepartmentService departmentService,
                         ApplicationEventPublisher eventPublisher,
                         JavaMailSender mailSender) {
        //this.captchaService = captchaService;
        this.userService = userService;
        this.departmentService = departmentService;
        this.eventPublisher = eventPublisher;
        this.mailSender = mailSender;
//        this.environment = environment;
//        this.mailSender = mailSender;
    }

    //@Layout("layouts/master")
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showRegistrationForm(Model model) {
        UserDto userDto = new UserDto();
        model.addAttribute("user", userDto);
        Iterable<Department> departments = departmentService.findAll();
        model.addAttribute("departments", departments);
//        return "fragments/register";
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView registerUserAccount(@ModelAttribute("user") @Valid final UserDto userDto, BindingResult bindingResult, Model model, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("validationErrors", bindingResult.getAllErrors());
//            if (!userDto.getPassword().equals(userDto.getMatchingPassword())) {
//                model.addAttribute("passNoMatch", "Passwords don't match!");
//            }
            model.addAttribute("user", userDto);
            return new ModelAndView("fragments/register", "user", userDto);
        } else {
// TODO: add captcha
//            try {
//                String response = httpServletRequest.getParameter("g-recaptcha-response");
//                captchaService.processResponse(response, getClientIP(httpServletRequest));
//            } catch (CaptchaException e) {
//                return new ModelAndView("fragments/register", "user", userDto);
//            }

            User registered = userService.registerNewUser(userDto);

            if (registered == null) {
                bindingResult.rejectValue("email", "message.regError");
            }

            if (bindingResult.hasErrors()) {
                return new ModelAndView("register", "user", userDto);
            } else {
                String appUrl = httpServletRequest.getProtocol() + "://" + httpServletRequest.getServerName() + ":" + httpServletRequest.getServerPort();
                String activationCode = registered.getActivationCode();
                String userEmail = registered.getUsername();
                String subject = "Activate your account";
                SimpleMailMessage email = new SimpleMailMessage();
                email.setFrom("kicekomilenkoski@gmail.com");
                email.setTo(userEmail);
                email.setSubject(subject);
                email.setText("Activate your account using the following code" + " \n" + activationCode);
                mailSender.send(email);
//                try {
//                    String appUrl = httpServletRequest.getProtocol() + "://" + httpServletRequest.getServerName() + ":" + httpServletRequest.getServerPort();
//                    String activationCode = registered.getActivationCode();
//                    String userEmail = registered.getUsername();
//                    String subject = "Activate your account";
//                    SimpleMailMessage email = new SimpleMailMessage();
//                    email.setFrom("kicekomilenkoski@gmail.com");
//                    email.setTo(userEmail);
//                    email.setSubject(subject);
//                    email.setText("Activate your account using the following code" + " \n" + activationCode);
//                    mailSender.send(email);
//                } catch (Exception me) {
//                    bindingResult.rejectValue("email", "message.regError");
//                    return new ModelAndView("register", "user", userDto);
//                }

                model.addAttribute("user", registered);


                //return new ModelAndView("fragments/confirmation-email", "user", registered);
                return new ModelAndView("register", "user", new UserDto());
            }

        }
    }

    @RequestMapping(value = "/change_password", method = RequestMethod.GET)
    public String resetPassword(@RequestParam(required = false) String email, Model model) {
        if (email != null) {
//            User user = userService.findByUsername(email);
//            if (!user.isPresent()) {
//                throw new UserNotFoundException();
//            }

//            String token = UUID.randomUUID().toString();
//            userService.createPasswordResetTokenForUser(user.get(), token);
//
//            // Send reset password email
//            mailSender.send(constructResetTokenEmail("http://localhost:8080", token, user.get()));
            String newPassword = userService.changePassword(email);
            if (newPassword != null){
                model.addAttribute("message", newPassword);
                String subject = "Reset Password";
                SimpleMailMessage mail = new SimpleMailMessage();
                mail.setFrom("kicekomilenkoski@gmail.com");
                mail.setTo(email);
                mail.setSubject(subject);
                mail.setText("Your new password is: " + " \n" + newPassword);
                model.addAttribute("message", "Your new password" +
                        "was sent to your email");
                mailSender.send(mail);

            }
            else {
                model.addAttribute("message", "Invalid user");
            }

        }
        return "change_password";
    }

    @RequestMapping(value = "/reset_password", method = RequestMethod.POST)
    public String resetPassword(@RequestParam(required = true) String newPassword,
                                @RequestParam(required = true) String matchingPassword,
                                Principal principal,
                                Model model) {

        String username = principal.getName();
        User user = userService.findByUsername(username);
        String pass = userService.resetPassword(user, newPassword, matchingPassword);

        return "redirect:/logout";
    }

    @RequestMapping(value = "/reset_password", method = RequestMethod.GET)
    public String resetPassword() {
        return "reset_password";
    }

    @RequestMapping(value = "/activate", method = RequestMethod.GET)
    public String resetPassword(@RequestParam(required = false) String email, @RequestParam(required = false) String code,Model model) {
        if (email != null) {
            boolean flag = userService.activateUser(email,code);
            if (flag){
                model.addAttribute("message", "Your account has been activated");
            }
            else {
                model.addAttribute("message", "Invalid code");
            }

        }
        return "activate";
    }

    @GetMapping("/get_name")
    @ResponseBody
    public String getName(Principal principal){
        String username = principal.getName();
        User user = userService.findByUsername(username);
        return user.firstName;
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/chatroom")
    public String chatroom() {
        return "index";
    }

    @GetMapping("/profile")
    public String profile(Model model, Principal principal) {

        String userName = principal.getName();
        User u = userService.findByUsername(userName);
        UserEdit userEdit = new UserEdit();
        userEdit.username = u.username;
        userEdit.firstName = u.firstName;
        userEdit.lastName = u.lastName;
        userEdit.birthDate = u.birthDate;
        userEdit.gender = u.gender.toString();
        model.addAttribute("user", userEdit);
        return "profile";
    }

    @RequestMapping(value = "/edit-user", method = RequestMethod.POST)
    public String editUser(@ModelAttribute("user") @Valid final UserEdit userEdit,
                           BindingResult bindingResult,
                           Model model,
                           HttpServletRequest httpServletRequest) {
        User u = userService.findByUsername(userEdit.username);
        u.firstName = userEdit.firstName;
        u.lastName = userEdit.lastName;
        u.birthDate = userEdit.birthDate;
        u.gender = userEdit.gender.equals("MALE") ? Gender.MALE : Gender.FEMALE;
        userService.save(u);
        model.addAttribute("user", userEdit);
        //String test = httpServletRequest.getRequestURI();
        return "profile";
    }

    @RequestMapping(value = "/jsonUsers", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void update(@RequestBody UserEdit user) {
//        if(video.id == null){
//            return service.validateUrlAndCreateVideo(
//                    video.title,
//                    video.url,
//                    video.description,
//                    video.category.id);
//        }
//        else{
            userService.updateUser(user.username, user.firstName,
                    user.lastName, user.gender, user.birthDate);
            //return new UserEdit();
        //}
    }



        @GetMapping("/users")
    public String userList(Model model,
                           @RequestParam(required = false, defaultValue = "") String firstName,
                           @RequestParam(required = false, defaultValue = "") String lastName,
                           @RequestParam(required = false, defaultValue = "") String query,
                           @RequestParam(required = false, defaultValue = "0") Integer page,
                           @RequestParam(required = false, defaultValue = "3") Integer size,
                           @RequestParam(required = false, defaultValue = "") String username) {
//        Iterable<User> users = userService.findAll();
//        model.addAttribute("users", users);
        Page<User> userList;
        User u = new User();
        UserEdit userEdit = new UserEdit();
        if(!username.equals("")){
            u = userService.findByUsername(username);
            userEdit.username = u.username;
            userEdit.firstName = u.firstName;
            userEdit.lastName = u.lastName;
            userEdit.birthDate = u.birthDate;
            userEdit.gender = u.gender.toString();
        }
        model.addAttribute("user", u);
        userList = userService.findAll(new PageRequest(page, size));
        model.addAttribute("prevPageRequest", buildURIFromParams(query, userList.isFirst() ? 0: userList.getNumber() - 1, size));
        model.addAttribute("nextPageRequest", buildURIFromParams(query, userList.isLast() ? userList.getNumber() : userList.getNumber() + 1, size));
        model.addAttribute("query", query);
        model.addAttribute("users", userList.getContent());
        model.addAttribute("pageNumber", userList.getNumber());
        model.addAttribute("prevPage", userList.isFirst() ? 0: userList.getNumber() - 1);
        model.addAttribute("nextPage", userList.isLast() ? userList.getNumber() : userList.getNumber() + 1);
        model.addAttribute("hasNext", userList.hasNext());
        model.addAttribute("hasPrev", userList.hasPrevious());
        model.addAttribute("page", page);
        model.addAttribute("size", size);


        return "users";
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public String users(Model model,
                        @RequestParam(required = false, defaultValue = "") String firstName,
                        @RequestParam(required = false, defaultValue = "") String lastName,
                        @RequestParam(required = false, defaultValue = "") String query,
                        @RequestParam(required = false, defaultValue = "0") Integer page,
                        @RequestParam(required = false, defaultValue = "3") Integer size
                        ){
        Pageable pageable = new PageRequest(0, 3);

        User user = new User();
        //user.firstName = "M";
        user.firstName = firstName;
        user.lastName = lastName;
        //user.activated = true;
        //user.id = 2;



        ExampleMatcher exampleMatcher = ExampleMatcher
                .matching()
                .withIgnoreCase()
                .withIgnoreNullValues()
                .withStringMatcher(ExampleMatcher.StringMatcher.STARTING)
                .withIgnorePaths("id", "activated");

        Example<User> videoExample = Example.of(user, exampleMatcher);

        Page<User> userList = userService.findAll(videoExample, new PageRequest(0, 3));
        model.addAttribute("prevPageRequest", buildURIFromParams(query, userList.isFirst() ? 0: userList.getNumber() - 1, size));
        model.addAttribute("nextPageRequest", buildURIFromParams(query, userList.isLast() ? userList.getNumber() : userList.getNumber() + 1, size));
        model.addAttribute("query", query);
        model.addAttribute("users", userList.getContent());
        model.addAttribute("pageNumber", userList.getNumber());
        model.addAttribute("prevPage", userList.isFirst() ? 0: userList.getNumber() - 1);
        model.addAttribute("nextPage", userList.isLast() ? userList.getNumber() : userList.getNumber() + 1);
        model.addAttribute("hasNext", userList.hasNext());
        model.addAttribute("hasPrev", userList.hasPrevious());
        model.addAttribute("page", page);
        model.addAttribute("size", size);

        return "users";
    }

    public static String buildURIFromParams(String searchQuery, Integer page, Integer size){
        return "/users?query=" + searchQuery + "&page=" + page + "&size=" + size;
    }



}
