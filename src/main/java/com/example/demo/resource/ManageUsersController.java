package com.example.demo.resource;

import com.example.demo.model.UserEdit;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.jws.soap.SOAPBinding;
import java.security.Principal;

@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class ManageUsersController {
    private final UserService userService;


    @Autowired
    public ManageUsersController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/userFB")
    public Principal user(Principal principal) {
        return principal;
    }

    @RequestMapping(value = "/jsonUsers", method = RequestMethod.POST)
    public UserEdit update(@RequestBody UserEdit user) {
//        if(video.id == null){
//            return service.validateUrlAndCreateVideo(
//                    video.title,
//                    video.url,
//                    video.description,
//                    video.category.id);
//        }
//        else{
        userService.updateUser(user.username, user.firstName,
                user.lastName, user.gender, user.birthDate);
        return null;
        //return new UserEdit();
        //}
    }

    @RequestMapping(value = "users/{username}", method = RequestMethod.DELETE)
    public void removeVideo(@PathVariable String username) {
        userService.removeUser(username);
    }

    @GetMapping("/get_name")
    public String getName(Principal principal){
        return "Hello" + principal.getName();
    }
}
