package com.example.demo.persistence;

import com.example.demo.model.Department;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

public interface DepartmentRepository extends CrudRepository<Department, Long> {
    Department save(Department department);

    Iterable<Department> findAll();

    Department findByName(String name);
}
