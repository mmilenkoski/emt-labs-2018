package com.example.demo.persistence;

import com.example.demo.model.User;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {

    User save(User user);

    //public User findByEmail(String email);

    User findByUsername(String username);
    Iterable<User> findAll();
    Page<User> findAll(Pageable pageable);
    Page<User> findAll(Example<User> example, Pageable pageable);
}
