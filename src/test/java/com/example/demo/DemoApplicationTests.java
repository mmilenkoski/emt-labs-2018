package com.example.demo;

import com.example.demo.model.Department;
import com.example.demo.model.User;
import com.example.demo.model.UserDto;
import com.example.demo.persistence.UserRepository;
import com.example.demo.service.DepartmentService;
import com.example.demo.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Random;
import java.util.function.Consumer;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	private static Logger logger = LoggerFactory.getLogger(DemoApplicationTests.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private DepartmentService departmentService;

	@Test
	public void contextLoads() {
	}

	@Test
	public void registration(){
		for (int i = 0; i< 20; i++){
			UserDto user = new UserDto();
			user.email = "user"+ i + "@gmail.com";
			logger.info(user.email);
			user.firstName = "Martin";
			user.lastName = "Milenkoski";
			user.password = "user";
			user.gender = "MALE";
			user.birthDate = LocalDate.now();
			user.department = "Accounting";
			//user.registrationDate = LocalDateTime.now();
			User u = userService.registerNewUser(user);
		}
//		UserDto user = new UserDto();
//		user.email = "user1@gmail.com";
//		user.firstName = "Martin";
//		user.lastName = "Milenkoski";
//		user.password = "test";
//		user.gender = "male";
//		user.birthDate = LocalDate.now();
//		//user.registrationDate = LocalDateTime.now();
//		User u = userService.registerNewUser(user);
//		logger.debug("################ {}", u.toString());
//		System.out.println(u.toString());
	}

	@Test
	public void createDepartment(){
		Department department = new Department();
		department.name = "Production";
		departmentService.create(department);
	}

	@Test
	public void activation(){
		String username = "martin@gmail.com";
		String code = "HQAUA2A891DOO24FZ6";
        User u = userRepository.findByUsername(username);
        assert (!u.activated);
		userService.activateUser(username, code);
		u = userRepository.findByUsername(username);
		assert (u.activated);
		logger.info("SUCCESS");
	}

	@Test
	public void changePassword(){
		String username = "martin@gmail.com";
		User u = userRepository.findByUsername(username);
		String password = userService.changePassword(username);
		assert (!u.getPassword().equals(password));
		logger.info("######### old: {}, new: {}", u.getPassword(), password);
	}

	private static Consumer<User> videoPrintingConsumerWithoutTags = u -> {
		logger.info("\tuser username: {}", u.username);
		logger.info("\tuser first name: {}", u.firstName);
		logger.info("\tuser last name: {}", u.lastName);

	};

	@Test
	public void testFetchPage() {
		logger.info("start find all videos");
		Pageable pageable = new PageRequest(0, 3);
		Page<User> res = userRepository.findAll(pageable);
		logger.info("total pages: {}; total elements: {}",
				res.getTotalPages(),
				res.getTotalElements()
		);
		res.forEach(videoPrintingConsumerWithoutTags);

		while (res.hasNext()) {
			Pageable next = res.nextPageable();
			res = userRepository.findAll(next);
			logger.info("total pages: {}; total elements: {}",
					res.getTotalPages(),
					res.getTotalElements()
			);
			res.forEach(videoPrintingConsumerWithoutTags);
		}


		logger.info("end find all videos");
	}

	@Test
	public void testSearchByExample() {
		logger.info("start find videos by example");
		Pageable pageable = new PageRequest(0, 3);

		User user = new User();
		//user.firstName = "M";
		user.firstName = "M";
		//user.activated = true;
		//user.id = 2;



		ExampleMatcher exampleMatcher = ExampleMatcher
				.matching()
				.withIgnoreCase()
				.withIgnoreNullValues()
				.withStringMatcher(ExampleMatcher.StringMatcher.STARTING)
				.withIgnorePaths("id", "activated");

		Example<User> videoExample = Example.of(user, exampleMatcher);

		Page<User> res = userRepository.findAll(videoExample, new PageRequest(0, 3));
		//Page<User> res = userRepository.findAll(pageable);
		logger.info("total pages: {}; total elements: {}",
				res.getTotalPages(),
				res.getTotalElements()
		);
		res.forEach(videoPrintingConsumerWithoutTags);
		logger.info("end find videos by example");
	}




}
