package com.example.demo;

import com.example.demo.model.Gender;
import com.example.demo.model.User;
import com.example.demo.model.UserDto;
import com.example.demo.persistence.UserRepository;
import com.example.demo.service.UserService;
import com.example.demo.service.impl.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;

import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase(replace = NONE)
public class DemoTest {

    private static Logger logger = LoggerFactory.getLogger(DemoTest.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Before
    public void setUp(){

    }

    @Test
    public void test1(){
        UserDto user = new UserDto();
        user.email = "martin@gmail.com";
        user.firstName = "Martin";
        user.firstName = "Milenkoski";
        user.password = "abcdef123";
        user.gender = "male";
        //user.birthDate = LocalDate.now();
        User u = userService.registerNewUser(user);
        logger.debug("################ {}", u.toString());
    }
}
